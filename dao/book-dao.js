const sqlite = require('sqlite3').verbose()
const db = new sqlite.Database('./db/database.sqlite')

class BookDAO {

  save(book) {
    return new Promise((resolve, reject) => {
      db.serialize(() => {
        const stmt = db.prepare("INSERT INTO books (title, price, author, isbn, number_of_pages, category) VALUES (?, ?, ?, ?, ?, ?)")
        const params = [book.title, book.price, book.author, book.isbn, book.numberOfPages, book.category]

        stmt.run(params, (err) => {
          if(err) {  
            reject(err)
          }
        })

        stmt.finalize(() => resolve())
      })
    })
  }

  allBooks() {
    return new Promise((resolve, reject) => {
      db.serialize(() => {
        const stmt = db.prepare("SELECT * FROM books")

        stmt.all((err, rows) => {
          if(err) {
            reject(err)
          }

          const books = rows.map(row => {
            const book = new Book(row.title, row.price, row.author, row.isbn, row.numberOfPages, row.category)
            book.id = row.id

            return book
          })

          resolve(books)
        })
        
        stmt.finalize()
      })
    })
  }

  bookWithId(id) {
    return new Promise((resolve, reject) => {
      db.serialize(() => {
        const stmt = db.prepare("SELECT * FROM books WHERE id = ?")

        stmt.each(id, (err, row) => {
          if(err) {
            reject(err)
          }

          const book = new Book(row.title, row.price, row.author, row.isbn, row.numberOfPages, row.category)
          book.id = row.rowid

          resolve(book)
        })

        stmt.finalize()
      })
    })
  }
}

module.exports = BookDAO

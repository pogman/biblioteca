class Book {
  constructor(title, price, author, isbn, numberOfPages, category) {
    this.title = title
    this.price = price
    this.author = author
    this.isbn = isbn
    this.numberOfPages = numberOfPages
    this.category = category
  }
}

module.exports = Book
